package com.example.rany.notificationdemo.notification_model;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.example.rany.notificationdemo.MainActivity;
import com.example.rany.notificationdemo.R;
import com.example.rany.notificationdemo.ResultActivity;

import java.util.List;

public class NotificationModel {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addChannerl(Context context){
        NotificationChannel channel = new NotificationChannel("channel_1",
                "my_channel", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager cManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        cManager.createNotificationChannel(channel);
    }

    public NotificationModel() {
    }

    // Simple Pendingintent notification
    public NotificationModel(Context context, String title, String des,
                             int icon, Bitmap largIcon, PendingIntent intent){
        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }
        builder.setContentTitle(title)
                .setContentText(des)
                .setSmallIcon(icon)
                .setLargeIcon(largIcon)
                .setContentIntent(intent);
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }
    // Multi Action notification
    public NotificationModel(Context context, String title, String des,
                             int icon, Bitmap largIcon, List<ActionNotification> actions){
        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }
        Intent i = new Intent(context.getApplicationContext(), ResultActivity.class);
        i.putExtra("name", "Welcome");

        PendingIntent intent = PendingIntent.getActivity(context.getApplicationContext(), 1,
                i, PendingIntent.FLAG_UPDATE_CURRENT);

        for(ActionNotification action : actions){
            builder.addAction(action.getIcon(), action.getName(), action.getPendingIntent());
        }
        builder.setContentTitle(title)
                .setContentText(des)
                .setSmallIcon(icon)
                .setLargeIcon(largIcon);
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    // Big picture style notification
    public NotificationModel(Context context, String title, String des,
                             int icon, Bitmap largIcon, PendingIntent intent, Bitmap largePic){
        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }
        builder.setContentTitle(title)
                .setContentText(des)
                .setSmallIcon(icon)
                .setLargeIcon(largIcon)
                .setContentIntent(intent)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(largePic));
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) (System.currentTimeMillis()/1000);
        manager.notify(id, builder.build());
    }

    // Big picture style notification
    public NotificationModel(Context context, String title, String des,
                             int icon, Bitmap largIcon, PendingIntent intent, String bigText){
        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }
        builder.setContentTitle(title)
                .setContentText(des)
                .setSmallIcon(icon)
                .setLargeIcon(largIcon)
                .setContentIntent(intent)
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(bigText));
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) (System.currentTimeMillis()/1000);
        manager.notify(id, builder.build());
    }

    // Custom notification
    public void customeNotification(Context context, String title, String des, int icon){

        NotificationCompat.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.custome_notification);

        remoteViews.setImageViewResource(R.id.imgNoti, icon);
        remoteViews.setTextViewText(R.id.tvTitle, title);
        remoteViews.setTextViewText(R.id.tvDes, des);

        builder.setSmallIcon(R.drawable.ic_filter_vintage_black_24dp)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(remoteViews);
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) (System.currentTimeMillis()/1000);
        manager.notify(id, builder.build());

    }

}
