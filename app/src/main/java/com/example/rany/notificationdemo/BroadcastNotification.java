package com.example.rany.notificationdemo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

public class BroadcastNotification extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder builder;

        Intent intent1 = new Intent(context, ResultActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context
                , 1
                , intent1
                , PendingIntent.FLAG_UPDATE_CURRENT);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = new NotificationCompat.Builder(context, "channel_1");
        }
        else{
            builder = new NotificationCompat.Builder(context);
        }

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.custome_notification);

        remoteViews.setImageViewResource(R.id.imgNoti, R.drawable.panda);
        remoteViews.setTextViewText(R.id.tvTitle, "Low Battery");
        remoteViews.setTextViewText(R.id.tvDes, "Please charge your phone !");

        builder.setSmallIcon(R.drawable.ic_filter_vintage_black_24dp)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(remoteViews)
                .setContentIntent(pendingIntent);

        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) (System.currentTimeMillis()/1000);
        manager.notify(id, builder.build());


    }
}
