package com.example.rany.notificationdemo.notification_model;

import android.app.PendingIntent;

public class ActionNotification {

    private int icon;
    private String name;
    private PendingIntent pendingIntent;

    public ActionNotification(int icon, String name, PendingIntent pendingIntent) {
        this.icon = icon;
        this.name = name;
        this.pendingIntent = pendingIntent;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
    }
}
