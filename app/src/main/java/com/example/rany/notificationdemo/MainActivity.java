package com.example.rany.notificationdemo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.rany.notificationdemo.notification_model.ActionNotification;
import com.example.rany.notificationdemo.notification_model.NotificationModel;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    NotificationModel noti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.btnNotification)
    public void simpleNotification(){

        Intent i = new Intent(this, ResultActivity.class);
        i.putExtra("name", "Welcome");

        PendingIntent intent = PendingIntent.getActivity(this, 1,
                i, PendingIntent.FLAG_UPDATE_CURRENT);

        ArrayList<ActionNotification> actions = new ArrayList<>();
        actions.add(new ActionNotification(R.drawable.ic_filter_vintage_black_24dp,
                "Open", intent));
        actions.add(new ActionNotification(R.drawable.ic_filter_vintage_black_24dp,
                "Close", intent));
        actions.add(new ActionNotification(R.drawable.ic_filter_vintage_black_24dp,
                "Reply", intent));

         noti = new NotificationModel(
                getApplicationContext(),
                "Hello world",
                "How are you?How are you?How are you?How are you?How are you?How are you?How are you?",
                R.drawable.ic_filter_vintage_black_24dp,
                BitmapFactory.decodeResource(getResources(), R.drawable.panda),
                 actions

        );
    }

    @OnClick(R.id.btnBigPicture)
    public void bigPictureNotification(){
        Intent i = new Intent(this, ResultActivity.class);
        i.putExtra("name", "Welcome");

        PendingIntent intent = PendingIntent.getActivity(this, 1,
                i, PendingIntent.FLAG_UPDATE_CURRENT);

        noti = new NotificationModel(
                getApplicationContext(),
                "Big picture style",
                "How is big picture style look like?",
                R.drawable.ic_filter_vintage_black_24dp,
                BitmapFactory.decodeResource(getResources(), R.drawable.panda),
                intent,
                BitmapFactory.decodeResource(getResources(), R.drawable.panda)
        );
    }

    @OnClick(R.id.btnCustom)
    public void customNotication(){
        noti = new NotificationModel();
        noti.customeNotification(getApplicationContext(), "Custome",
                "Custome notification", R.drawable.panda);
    }

    @OnClick(R.id.btnBigText)
    public void bigTextNotification(){
        Intent i = new Intent(this, ResultActivity.class);
        i.putExtra("name", "Welcome");

        PendingIntent intent = PendingIntent.getActivity(this, 1,
                i, PendingIntent.FLAG_UPDATE_CURRENT);
        noti = new NotificationModel(
                getApplicationContext(),
                "Hello world",
                "Hi",
                R.drawable.ic_filter_vintage_black_24dp,
                BitmapFactory.decodeResource(getResources(), R.drawable.panda),
                intent,
                "Field and method binding for Android views which uses annotation processing to generate boilerplate code for you."

        );
    }

}
